# %%
import pandas
import numpy
import colorlover
from IPython.core.display import display
import plotly_pandas as ply_pd


# %%
filename = '20220101_portfolio.csv'

loanbook = pandas.read_csv(filename)

# %%
loanbook.loc[pandas.DatetimeIndex(loanbook['Start date']).month <= 6, 'Cohort'] = 'H1'
loanbook.loc[pandas.DatetimeIndex(loanbook['Start date']).month > 6, 'Cohort'] = 'H2'

loanbook['Cohort'] = loanbook['Cohort'] + '-' + pandas.DatetimeIndex(loanbook['Start date']).year.astype(str).str[2:]

# %%

loanbook['Start date'] = pandas.to_datetime(loanbook['Start date']) 
loanbook['First failed payment date'] = pandas.to_datetime(loanbook['First failed payment date']) 
loanbook['Months since origination'] = ((loanbook['First failed payment date'] - loanbook['Start date']) / numpy.timedelta64(1, 'M')).round(0)

# %%

defaulted_by_group = loanbook.groupby('Loan id')[['Cohort', 'Months since origination', 'Sum of late payments']].first().groupby(['Cohort', 'Months since origination'])['Sum of late payments'].sum().unstack('Cohort').fillna(0).cumsum()

group_count = loanbook.groupby(['Months since origination', 'Cohort'])[
            'Loan amount'].sum().unstack('Cohort').max()

cum_cdr = (defaulted_by_group / group_count) / 100

cum_cdr
# %%
vintages_subset = ['H2-15',
                   'H1-16', 'H2-16',
                   'H1-17', 'H2-17',
                   'H1-18', 'H2-18',
                   'H1-19', 'H2-19',
                   'H1-20', 'H2-20',
                   'H1-21']
# %%
sel_color_scales = ['Greys', 'YlOrRd', 'Blues', 'Greens', 'Reds', 'Purples']

sel_vintage_groups = [['H%d-%d' % (q, y)
                       for q in range(1, 3)] for y in range(15, 21)]

print(sel_vintage_groups)

sel_vintage_color_settings_3 = {k: {'line': {'color': colorlover.scales['5']['seq'][sel_color_scales[i]][j+1][:-1]
                                             + (',0.1)' if not k.startswith('2019') else ',1.0)')},
                                             'opacity' : 0.5}
                                for i, col_group in enumerate(sel_vintage_groups)
                                for j, k in enumerate(col_group)}

for q in range(1, 5):
    sel_vintage_color_settings_3[f'H1-21'] = {
        'line': {'color': colorlover.scales['5']['seq'][sel_color_scales[-3]][q]}
    }


# %%
october_cum_cdr_chart = ply_pd.chart(cum_cdr[vintages_subset],
                                       layout={'yaxis': ply_pd.percent_axis({'title': 'Cumulative default rate',
                                                                             'range': [0, 0.026]},
                                                                             tick_precision = 1),
                                               'xaxis': {'title': 'Months since originated',
                                                         'range': [0, 60]}},
                                       column_settings=sel_vintage_color_settings_3,
                                       width=800,
                                       height=500)

cum_cdr.index.name = 'Months'
cum_cdr.columns.name = 'Vintage'
display(october_cum_cdr_chart)
